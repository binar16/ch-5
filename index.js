const express = require("express");
const bodyParser = require("body-parser")
const moduleList = require("./modules/module")

const port = 3000;
const app = express();

app.set("view engine", "ejs");
app.use(express.static("assets"));
app.use(bodyParser.json())

app.get("/", moduleList.land);

app.get("/login", moduleList.list);

app.post("/login", moduleList.check);

app.get("/game", moduleList.game);

app.listen(port, () => {
  console.log("app listen on port", port);
});
